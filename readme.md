# Metado API

Api para extração de metados de arquivos de imagens


Recursos disponíveis para acesso via API:
* [**Upload Imagem**](#reference/recursos/contatos)
* [**Busca Metadados**](#reference/recursos/produtos)

## Métodos
Requisições para a API devem seguir os padrões:
| Método | Descrição |
|---|---|
| `GET` | Retorna informações de um ou mais registros. |
| `POST` | Utilizado para criar um novo registro. |
| `PUT` | Atualiza dados de um registro ou altera sua situação. |
| `DELETE` | Remove um registro do sistema. |


## Respostas

| Código | Descrição |
|---|---|
| `201` | Metadado criaco com sucesso (success).|
| `200` | Requisição executada com sucesso (success).|
| `400` | Erros de validação ou os campos informados não existem no sistema.|
| `404` | Registro pesquisado não encontrado (Not found).|


## Listar
As ações de `listar` permitem o envio dos seguintes parâmetros:

| Parâmetro | Descrição |
|---|---|
| `field` | Filtra dados pelo campo da tabela do banco de dados. |
| `op` | Operação usada na filtragem do campo '==', '>', '<', '!=' etc..|
| `value` | Valor do campo a ser filtrado. |
| `deleted` | Campo booleado para obter registros deletados |


               }