import pytest
from app import create_app, db
from models.metadado import MetaDado
from src.test import get_random_string
from datetime import datetime


@pytest.fixture(scope="session", autouse=True)
def app():
    return create_app('testing')


@pytest.fixture(scope='function')
def client(app, db_4_test):
    client = app.test_client()
    return client


@pytest.fixture(scope='function')
def db_4_test(app):
    with app.app_context():
        db.drop_all()
        db.create_all()
        for num in range(30):
            metadado = MetaDado()
            metadado.Author = f'testing author {num}'
            metadado.Model = "Sony"
            metadado.filename = f'testing_file_{num}.jpg'
            metadado.save()

        for num in range(30, 50):
            metadado = MetaDado()
            metadado.Author = 'someAuthor'
            metadado.Model = "Samsung"
            metadado.Software = 'HDR+ 1.0.296349522np'
            metadado.filename = f'testing_file_{num}.jpg'
            metadado.save()

        for num in range(50, 70):
            metadado = MetaDado()
            metadado.Author = 'someAuthor2'
            metadado.Model = "LG"
            metadado.Software = 'HDR+ x'
            metadado.ExifImageWidth = 4000
            metadado.filename = f'testing_file_{num}.jpg'
            metadado.save()

        for num in range(70, 100):
            metadado = MetaDado()
            metadado.Author = 'someAuthor3'
            metadado.Model = "Pixel 3 XL"
            metadado.Software = 'HDR+2'
            metadado.ExifImageWidth = 3500
            metadado.Contrast = 0
            metadado.Saturation = 1
            metadado.Sharpness = 2
            metadado.filename = f'testing_file_{num}.jpg'
            metadado.save()
