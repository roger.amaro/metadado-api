import json
import logging
import sys
import uuid
from datetime import datetime, timezone

from config import app_config
from flask import Flask, g, request
from flask_migrate import Migrate

from src.models import db


def create_app(env_name):
    config = app_config[env_name]
    app = Flask(__name__)
    app.config.from_object(config)
    app.config['PROPAGATE_EXCEPTIONS'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    error_handler = logging.StreamHandler(sys.stderr)
    app.error_logger = logging.getLogger('api_error_log')
    app.error_logger.setLevel(logging.INFO)
    app.error_logger.addHandler(error_handler)

    @app.before_request
    def before_request():
        g.start = datetime.utcnow()
        g.request_id = uuid.uuid4().hex

    @app.after_request
    def after_request(response):
        time = datetime.utcnow() - g.start
        log = {
            'request_id': g.request_id,
            'type': 'access',
            'time': datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f %z'),

            'method': request.method,
            'path': request.full_path,
            'status': response.status,
            'duration': time.microseconds / 1000,
            'platform': request.user_agent.platform,
            'browser': request.user_agent.browser,
            'browser_version': request.user_agent.version,
            'user_agent_str': request.user_agent.string,
        }

        app.error_logger.info(json.dumps(log))
        return response

    db.init_app(app)
    Migrate(app, db)

    from src.views import metadata_blueprint, upload_blueprint

    app.register_blueprint(metadata_blueprint, url_prefix="/api/v1")
    app.register_blueprint(upload_blueprint, url_prefix="/api/v1")

    return app
