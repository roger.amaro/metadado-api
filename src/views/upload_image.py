import os

from PIL import Image, ExifTags
from flask import request, abort, Blueprint, make_response, jsonify
from werkzeug.utils import secure_filename

from config import Config
from src.models import MetaDado, MetaDadoSchema
from utils import allowed_file


def check_file_exists(filename):
    if os.path.isfile(os.path.join(Config.UPLOAD_FOLDER, filename)):
        return True
    return False


upload_blueprint = Blueprint('upload', __name__)


@upload_blueprint.route('/upload/', methods=['POST'])
def upload_file():
    file = request.files.get('image')

    if 'image' not in request.files or not file:
        abort(make_response(jsonify(message='No file part'), 400))

    if file.filename == '':
        abort(make_response(jsonify(message='No filename in image'), 400))

    if not allowed_file(file.filename):
        abort(make_response(jsonify(message='File extension is not allowed'), 400))

    filename = secure_filename(file.filename)
    if check_file_exists(filename):
        abort(make_response(jsonify(message=f'the file {filename} already uploaded'), 400))

    url_file = os.path.join(Config.UPLOAD_FOLDER, filename)

    file.save(url_file)

    img = Image.open(os.path.join(Config.UPLOAD_FOLDER, filename))
    exif = {ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS}

    try:
        exif.update(dict(filename=filename))
        schema = MetaDadoSchema().load(exif)
        meta_data = MetaDado(**schema)
        meta_data.save()

        return jsonify(dict(status="file has been processed successfully",
                            data=schema)), 201

    except Exception as err:
        abort(make_response(jsonify(message='database error :('), 400))
