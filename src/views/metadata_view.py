import os

from flask import Blueprint, abort, jsonify, request, make_response

from config import Config
from src.models import FilterSchema, MetaDado, MetaDadoSchema
from utils import OPERATORS

metadata_blueprint = Blueprint('metadado', __name__)


@metadata_blueprint.route('/metadado/', methods=['GET'])
def get_metadata_list():
    query = MetaDado.query
    args = FilterSchema().load(request.args) if request.args else None
    if args:
        field = args.get("field")
        op = args.get("op")
        value = args.get("val")
        deleted = args.get('deleted')

        if not hasattr(MetaDado, field):
            abort(make_response(jsonify(message=f"has no field {field}"), 400))

        field = getattr(MetaDado, field)
        operator = OPERATORS.get(op)
        if operator is None:
            abort(make_response(jsonify(message=f"Operator {op} doest exist"), 400))

        query = query.filter(operator(field, value))
        if not deleted or not isinstance(deleted, bool):
            query = query.filter_by(dh_fim_registro=None)

    schema = MetaDadoSchema(many=True).dump(query.all())
    return jsonify(schema), 200


@metadata_blueprint.route('/metadado/<int:id>', methods=['GET'])
def get_by_id(id):
    schema = MetaDadoSchema()
    metadado = MetaDado.query.filter_by(metadado_id=id) \
        .filter_by(dh_fim_registro=None).one_or_none()

    if not metadado:
        abort(make_response(jsonify(message=f"metadado with id {id} not found"), 404))

    return jsonify(schema.dump(metadado)), 200


@metadata_blueprint.route('/metadado/<int:id>', methods=['PUT'])
def update_metadado(id):
    schema = MetaDadoSchema()
    metadado = query = MetaDado.query.filter_by(metadado_id=id) \
        .filter_by(dh_fim_registro=None).one_or_none()

    if not metadado:
        abort(make_response(jsonify(message=f"metadado with id {id} not found"), 404))

    new_metadado = schema.load(request.get_json(silent=True))
    metadado.update(new_metadado)
    return jsonify(schema.dump(metadado)), 200


@metadata_blueprint.route('/metadado/<int:id>', methods=['DELETE'])
def delete_metadado(id):
    metadado = query = MetaDado.query.filter_by(metadado_id=id) \
        .filter_by(dh_fim_registro=None).one_or_none()

    if not metadado:
        abort(make_response(jsonify(message=f"metadado with id {id} not found"), 404))

    filename = metadado.filename
    file = Config.UPLOAD_FOLDER + "/" + filename
    if os.path.exists(file):
        os.remove(file)
    metadado.delete()
    return jsonify(message="successfully deleted"), 200
