from .urls import GET_METADADOS, UPLOAD_FILE


def test_filters_field_inexistente(client, data_regression):
    filters = dict(field='name', op='like', val='%y%', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 400
    data_regression.check(response.get_json())


def test_filters_by_filename(client):
    filters = dict(field='filename', op='==', val='testing_file_26.jpg', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    assert response.get_json()[0].get("filename") == 'testing_file_26.jpg'
    assert len(response.get_json()) == 1


def test_filters_by_model(client):
    filters = dict(field='Model', op='==', val='Sony', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    assert len(response.get_json()) == 30


def test_filters_by_author(client):
    filters = dict(field='Author', op='==', val='someAuthor2', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    assert len(response.get_json()) == 20


def test_filters_by_ExifImageWidth(client):
    filters = dict(field='ExifImageWidth', op='<', val='4000', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    assert len(response.get_json()) == 30


def test_filters_by_ExifImageWidth_equals(client):
    filters = dict(field='ExifImageWidth', op='==', val='4000', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    assert len(response.get_json()) == 20


def test_get_by_id(client):
    response = client.get(f'{GET_METADADOS}{30}')
    assert response.status_code == 200
    assert response.get_json().get("metadado_id") == 30


def test_update_metadado(client):
    response = client.get(f'{GET_METADADOS}{30}')
    assert response.status_code == 200

    metadado = response.get_json()
    metadado.update({"ApertureValue": "120"})

    response = client.put(f'{GET_METADADOS}{30}', json=metadado)
    assert response.status_code == 200

    response = client.get(f'{GET_METADADOS}{30}')
    assert response.status_code == 200
    assert response.get_json().get("ApertureValue") == '120'


def test_delete_metadado(client, data_regression):
    filename = 'test_1.jpg'
    data = {
        'image': (open(f'test/files_test/{filename}', 'rb'), filename)
    }

    response = client.post(UPLOAD_FILE, data=data)
    assert response.status_code == 201

    filters = dict(field='filename', op='==', val='test_1.jpg', deleted=False)
    response = client.get(GET_METADADOS, query_string=filters)
    assert response.status_code == 200
    id_metadado = response.get_json()[0].get("metadado_id")
    response = client.delete(f'{GET_METADADOS}{id_metadado}')
    assert response.status_code == 200

    response = client.get(f'{GET_METADADOS}{id_metadado}')
    assert response.status_code == 404
    data_regression.check(response.get_json())
