import os

from config import Config
from .urls import UPLOAD_FILE


def test_already_upload_jpeg_file(client, data_regression):
    filename = 'test.jpg'
    data = {
        'image': (open(f'test/files_test/{filename}', 'rb'), filename)
    }

    response = client.post(UPLOAD_FILE, data=data)
    assert response.status_code == 400
    data_regression.check(response.get_json())


def test_not_uploaded_jpeg_file(client, data_regression):
    filename = 'test_1.jpg'
    data = {
        'image': (open(f'test/files_test/{filename}', 'rb'), filename)
    }

    response = client.post(UPLOAD_FILE, data=data)
    assert response.status_code == 201
    file = Config.UPLOAD_FOLDER + "/" + filename
    if os.path.exists(file):
        os.remove(file)

    data_regression.check(response.get_json())


def test_upload_pdf_file(client, data_regression):
    filename = 'file_test_2.pdf'
    data = {
        'image': (open(f'test/files_test/{filename}', 'rb'), filename)
    }

    response = client.post(UPLOAD_FILE, data=data)
    assert response.status_code == 400
    data_regression.check(response.get_json())

