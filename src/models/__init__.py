from sqlalchemy import Column, DateTime
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True

    dh_inicio_registro = Column(DateTime(timezone=True), default=datetime.utcnow())
    dh_update = Column(DateTime(timezone=True), onupdate=datetime.utcnow())
    dh_fim_registro = Column(DateTime(timezone=True))

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        self.dh_fim_registro = datetime.utcnow()
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()


class FilterSchema(Schema):
    field = fields.String()
    op = fields.String()
    val = fields.String()
    deleted = fields.Boolean(required=False)


from .metadado import MetaDado, MetaDadoSchema
