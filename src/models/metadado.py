from marshmallow import Schema, fields, pre_load, post_dump
from sqlalchemy import Column, Integer, String, DateTime

from . import BaseModel


class MetaDado(BaseModel):
    metadado_id = Column(Integer(), primary_key=True)
    filename = Column(String(64), unique=True)
    Author = Column(String(50))
    ImageWidth = Column(Integer())
    ImageLength = Column(Integer())
    Make = Column(String(50))
    Model = Column(String(50))
    Orientation = Column(Integer())
    XResolution = Column(String(50))
    YResolution = Column(String(50))
    ResolutionUnit = Column(Integer())
    Software = Column(String(50))
    DateTime_ = Column(DateTime(timezone=True))
    YCbCrPositioning = Column(Integer())
    ExifOffset = Column(Integer())
    ExposureTime = Column(String(30))
    FNumber = Column(String(30))
    ExposureProgram = Column(Integer())
    ISOSpeedRatings = Column(Integer())
    ExifVersion = Column(Integer())
    DateTimeOriginal = Column(String(30))
    DateTimeDigitized = Column(String(30))
    ApertureValue = Column(String(30))
    MaxApertureValue = Column(String(30))
    MeteringMode = Column(Integer())
    Flash = Column(Integer())
    FocalLength = Column(String(30))
    SubsecTime = Column(String(50))
    SubsecTimeOriginal = Column(String(50))
    SubsecTimeDigitized = Column(String(50))
    FlashPixVersion = Column(String(50))
    ColorSpace = Column(Integer())
    ExifImageWidth = Column(Integer())
    ExifImageHeight = Column(Integer())
    ExifInteroperabilityOffset = Column(Integer())
    SensingMethod = Column(Integer())
    SceneType = Column(Integer())
    CustomRendered = Column(Integer())
    ExposureMode = Column(Integer())
    WhiteBalance = Column(Integer())
    DigitalZoomRatio = Column(String(30))
    FocalLengthIn35mmFilm = Column(Integer())
    SceneCaptureType = Column(Integer())
    Contrast = Column(Integer())
    Saturation = Column(Integer())
    Sharpness = Column(Integer())
    SubjectDistanceRange = Column(String(30))


class MetaDadoSchema(Schema):
    class Meta:
        unknown = 'EXCLUDE'

    SKIP_VALUES = {None}

    @pre_load
    def tuple_to_str(self, data, **kwargs):
        for (key, value) in data.items():
            if isinstance(value, tuple):
                data[key] = str(value)
        return data

    @post_dump
    def remove_skip_values(self, data, **kwargs):
        return {
            key: value for key, value in data.items()
            if value not in self.SKIP_VALUES
        }

    metadado_id = fields.Integer(dump_only=True)
    filename = fields.String(required=True)

    dh_inicio_registro = fields.DateTime(dump_only=True)
    dh_fim_registro = fields.DateTime(dump_only=True)
    dh_ultimo_update = fields.DateTime(dump_only=True)

    ImageWidth = fields.Integer(required=False)
    Author = fields.String(required=False)
    ImageLength = fields.Integer(required=False)
    Make = fields.String(required=False)
    Model = fields.String(required=False)
    Orientation = fields.Integer(required=False)
    XResolution = fields.String(required=False)
    YResolution = fields.String(required=False)
    ResolutionUnit = fields.Integer(required=False)
    Software = fields.String(required=False)
    DateTime = fields.DateTime(data_key="DateTime_", required=False)
    YCbCrPositioning = fields.Integer(required=False, allow_none=True)
    ExifOffset = fields.Integer(required=False)
    ExposureTime = fields.String(required=False)
    FNumber = fields.String(required=False)
    ExposureProgram = fields.Integer(required=False)
    ISOSpeedRatings = fields.Integer(required=False)
    ExifVersion = fields.Integer(required=False)
    DateTimeOriginal = fields.String(required=False)
    DateTimeDigitized = fields.String(required=False)
    ApertureValue = fields.String(required=False)
    MaxApertureValue = fields.String(required=False)
    MeteringMode = fields.Integer(required=False)
    Flash = fields.Integer(required=False)
    FocalLength = fields.String(required=False)
    SubsecTime = fields.String(required=False)
    SubsecTimeOriginal = fields.String(required=False)
    SubsecTimeDigitized = fields.String(required=False)
    FlashPixVersion = fields.String(required=False)
    ColorSpace = fields.Integer(required=False)
    ExifImageWidth = fields.Integer(required=False)
    ExifImageHeight = fields.Integer(required=False)
    ExifInteroperabilityOffset = fields.Integer(required=False)
    SensingMethod = fields.Integer(required=False)
    SceneType = fields.Integer(required=False)
    CustomRendered = fields.Integer(required=False)
    ExposureMode = fields.Integer(required=False)
    WhiteBalance = fields.Integer(required=False)
    DigitalZoomRatio = fields.String(required=False)
    FocalLengthIn35mmFilm = fields.Integer(required=False)
    SceneCaptureType = fields.Integer(required=False)
    Contrast = fields.Integer(required=False)
    Saturation = fields.Integer(required=False, allow_none=True)
    Sharpness = fields.Integer(required=False)
    SubjectDistanceRange = fields.Integer(required=False)
