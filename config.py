import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(), override=True)


class Config(object):
    CSRF_ENABLED = True
    SECRET = 'hfdhfhsdfjhp948759843hfis1223'
    BCRYPT_LOG_ROUNDS = 12
    UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'files')
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


class DevelopmentConfig(Config):
    TESTING = False
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_TEST_DATABASE_URI", 'sqlite://')


class ProductConfig(Config):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")


app_config = {
    'development': DevelopmentConfig(),
    'testing': TestingConfig(),
    'production': ProductConfig()
}
